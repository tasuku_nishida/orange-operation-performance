
#import "RNPerformance.h"

@implementation RNPerformance

RCT_EXPORT_MODULE()

RCT_EXPORT_METHOD(getCurrentResidentSize:(RCTResponseSenderBlock)callback)
{
    struct task_basic_info info;
    mach_msg_type_number_t basicInfoCount  = TASK_BASIC_INFO_COUNT;
    
    if (task_info(current_task(), TASK_BASIC_INFO, (task_info_t)&info, &basicInfoCount) != KERN_SUCCESS) {
        callback(@[]);
    }
    double mem = (double)info.resident_size / 1024 /1024;
    NSString *residentSize = [NSString stringWithFormat:@"%.2lf", mem];
    
    callback(@[residentSize]);
}

@end
