using ReactNative.Bridge;
using System;
using System.Collections.Generic;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;

namespace Performance.RNPerformance
{
    /// <summary>
    /// A module that allows JS to share data.
    /// </summary>
    class RNPerformanceModule : NativeModuleBase
    {
        /// <summary>
        /// Instantiates the <see cref="RNPerformanceModule"/>.
        /// </summary>
        internal RNPerformanceModule()
        {

        }

        /// <summary>
        /// The name of the native module.
        /// </summary>
        public override string Name
        {
            get
            {
                return "RNPerformance";
            }
        }
    }
}
